;;;; json.lisp - A JSON encoder/decoder module.
;;;;
;;;; Copyright (C) 2019 Momozor <skelic3@gmail.com>.
;;;; Copyright (c) 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010,
;;;; 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018 Python Software Foundation.
;;;; All Rights Reserved.
;;;;
;;;; For license, see LICENSE file for further details.

(in-package :cl-user)
(defpackage cl-mana.json
  (:use :cl)
  (:import-from :cl-json
                :encode-json-to-string
                :decode-json-from-string)
  (:export :dumps
           :loads))
(in-package :cl-mana.json)

;;; Functions.

(defun dumps (sequence)
  "Encode and dump JSON from Lisp sequence to string."
  (encode-json-to-string sequence))

(defun loads (sequence)
  "Decode JSON from a string sequence."
  (decode-json-from-string sequence))
