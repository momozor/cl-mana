;;;; random.lisp - A randomizer module.
;;;;
;;;; Copyright (C) 2019 Momozor <skelic3@gmail.com>.
;;;; Copyright (c) 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010,
;;;; 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018 Python Software Foundation.
;;;; All Rights Reserved.
;;;;
;;;; For license, see LICENSE file for further details.

(in-package :cl-user)
(defpackage cl-mana.random
  (:use :cl)
  (:export :choice))
(in-package :cl-mana.random)

;;; Generic declarations.

(defgeneric choice (sequence)
  (:documentation "Return random item from a sequence of list or vector/array."))

;;; Generic functions.

(defmethod choice ((sequence list))
  (let ((i (random
            (length sequence))))
    (nth i sequence)))

(defmethod choice ((sequence vector))
  (let ((i (random
            (length sequence))))
    (aref sequence i)))
