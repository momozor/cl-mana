;;;; string.lisp - A string utility module.
;;;;
;;;; Copyright (C) 2019 Momozor <skelic3@gmail.com>.
;;;; Copyright (c) 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010,
;;;; 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018 Python Software Foundation.
;;;; All Rights Reserved.
;;;;
;;;; For license, see LICENSE file for further details.

(in-package :cl-user)
(defpackage cl-mana.string
  (:use :cl)
  (:export :+ascii-lowercase+
           :+ascii-uppercase+
           :+digits+
           :+hexdigits+
           :+octdigits+
           :join
           :split
           :capwords))
(in-package :cl-mana.string)

;;; Constants.

(defparameter +ascii-lowercase+ "abcdefghijklmnopqrstuvwxyz")
(defparameter +ascii-uppercase+ "ABCDEFGHIJKLMNOPQRSTUVWXYZ")
(defparameter +digits+ "0123456789")
(defparameter +hexdigits+ "0123456789abcdefABCDEF")
(defparameter +octdigits+ "01234567")

;;; Functions.

(defun join (sequence &key (separator " "))
  "Join a list of strings into a single string."
  (cl-strings:join sequence :separator separator))

(defun split (str &key (separator " "))
  "Split a single string into multiple string."
  (cl-strings:split str separator))

(defun capwords (str &key (separator " "))
  "Turn a string into capitalized words."
  (join
   (mapcar #'string-capitalize
           (split str :separator separator))
   :separator " "))
