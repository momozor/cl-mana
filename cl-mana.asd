(defsystem "cl-mana"
  :version "0.1.0"
  :author "Momozor"
  :license "MIT"
  :depends-on ("alexandria"
               "cl-ppcre"
               "cl-strings"
               "cl-json")
  :components ((:module "src"
                        :components
                        ((:file "random")
                         (:file "string")
                         (:file "json"))))
  :description "A general-purpose library that aims to be a 'battery' for Lisp."
  :in-order-to ((test-op (test-op "cl-mana/tests"))))

(defsystem "cl-mana/tests"
  :author "Momozor"
  :license "MIT"
  :depends-on ("cl-mana"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for cl-mana"
  :perform (test-op (op c) (symbol-call :rove :run c)))
