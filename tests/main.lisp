;;;; main.lisp - Main test file.
;;;;
;;;; Copyright (C) 2019 Momozor <skelic3@gmail.com>.
;;;; Copyright (c) 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010,
;;;; 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018 Python Software Foundation.
;;;; All Rights Reserved.
;;;;
;;;; For license, see LICENSE file for further details.

(in-package :cl-user)
(defpackage cl-mana/tests/main
  (:use :cl)
  (:import-from :rove
                :deftest
                :testing
                :ok))
(in-package :cl-mana/tests/main)

;;; NOTE: To run this test file, execute `(asdf:test-system :cl-mana)' in your Lisp.

;;; String module.

(deftest +ascii-lowercase+
  (testing "+ascii-lowercase+ is equal to a-z"
    (ok (string=
         "abcdefghijklmnopqrstuvwxyz"
         cl-mana.string:+ascii-lowercase+))))

(deftest +ascii-uppercase+
  (testing "+ascii-uppercase+ is equal to A-Z"
    (ok (string=
         "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
         cl-mana.string:+ascii-uppercase+))))

(deftest +digits+
  (testing "+digits+ is equal to 0-9"
    (ok (string=
         "0123456789"
         cl-mana.string:+digits+))))

(deftest +hexdigits+
  (testing "+hexdigits+ is equal to 0123456789abcdefABCDEF"
    (ok (string=
         "0123456789abcdefABCDEF"
         cl-mana.string:+hexdigits+))))

(deftest +octdigits+
  (testing "+octdigits+ is equal to 01234567"
    (ok (string=
         "01234567"
         cl-mana.string:+octdigits+))))

(deftest join
  (testing "(join '(\"hello\" \"meow\" \"woof\")) returns \"hello meow woof\""
    (ok (string=
         "hello meow woof"
         (cl-mana.string:join '("hello" "meow" "woof"))))))

(deftest split
  (testing "(split \"hello meow woof\") returns '(\"hello\" \"meow\" \"woof\")"
    (ok (equal
         '("hello" "meow" "woof")
         (cl-mana.string:split "hello meow woof")))))

(deftest capwords
  (testing "(capwords \"welcome|to|jurassic|park\")"
    (ok (string=
         "Welcome To Jurassic Park"
         (cl-mana.string:capwords "welcome|to|jurassic|park"
                                  :separator #\|)))))

;;; JSON module.

(deftest dumps
  (testing "(dumps '((almet . 23) (ram . #\s))) should return
{\"almet\":\"23\",\"ram\":\"s\"}"
    (ok (string=
         "{\"almet\":23,\"ram\":\"s\"}"
         (cl-mana.json:dumps '((:almet . 23) (:ram . "s")))))))

(deftest loads
  (testing "loads"
      (ok (equal
           '((:almet . 23) (:ram . "s"))
           (cl-mana.json:loads "{\"almet\":23,\"ram\":\"s\"}")))))


;;; Random module (SKIPPED UNTIL BETTER STRATEGY IS FOUND).

;; TODO find a way to test these random functions
;;(deftest choice
;;  (testing "20 is in (choice '(10 20 30))"
;;    (ok (equal 20 (cl-mana.random:choice
;;                   (list 10 20 30 40 50)))))

;;  (testing "C is in (choice #(a b c))"
;;    (ok (eq 'c (cl-mana.random:choice
;;                (choice #(a b c)))))))
